#pragma once
class Encryption
{
public:
	Encryption();
	~Encryption();

/**
	* \brief 4 rakamlı sayıların şifrelenmesini yapmaktadır
	*/
	int encryption(int);
	/**
	* \brief 4 rakamlı sayıların şifre çözümünü yapmaktadır
	*/
	int decryption(int);
};
