#pragma once
#include <math.h>
/**
* @File Pose.h
* @Date Aralik, 2018
* @Author Eda Ayaz (edayazz.1@gmail.com)
*
* \brief Pose class 
*/
class Pose
{
private:
	float x;
	float y;
	float z;
public:
	/**
	*
	* @brief Milimetre cinsindeki x mesafesini donduren fonksiyon
	* @retval x mesafesi
	*
	**/
	float getX();
	/**
	*
	* @brief Milimetre cinsindeki x mesafesini set eden fonksiyon.
	* @param X : mesafe
	*
	**/
	void setX(float);
	/**
	*
	* @brief Milimetre cinsindeki y mesafesini donduren fonksiyon
	* @retval y mesafesi
	*
	**/
	float getY();
	/**
	*
	* @brief Milimetre cinsindeki y mesafesini set eden fonksiyon.
	* @param Y: mesafe
	*
	**/
	void setY(float);
	/**
	*
	* @brief Derece cinsindeki aci degiskenini set eder.
	*
	*
	**/
	float getTh();
	/**
	*
	* @brief Derece cinsindeki aci degiskenini dondurur.
	*
	* @retval th aci degeri
	*
	**/
	void setTh(float);
	/**
	*
	* @brief Verilen iki pozisyonun esit olup olmadigini kontrol eden fonksiyon.
	* @param Pose : karsilastirilacak pozisyon.
	*
	**/

	bool operator==(const Pose& other);
	/**
	*
	* @brief Verilen iki pozisyon degerlerini toplayan fonksiyondur.
	* @param Pose : karsilastirilacak pozisyon.
	*
	**/
	Pose operator+(const Pose& other);
	/**
	*
	* @brief Verilen iki pozisyon degerlerini cikaran fonksiyondur.
	* @param Pose : karsilastirilacak pozisyon.
	*
	**/
	Pose operator-(const Pose& other);

	Pose& operator+=(const Pose& other);
	Pose& operator-=(const Pose& other);
	/**
	*
	* @brief Verilen iki pozisyon degerlerinin buyuk veya kucuk oldugunu kontrol eden fonksiyon.
	* @param Pose : karsilastirilacak pozisyon.
	*
	**/
	bool operator<(const Pose& other);
	/**
	*
	* @brief Pose objesindeki aci ve mesafe degerlerini atayan fonksiyondur.
	*
	**/
	void getPose(float& _x, float& _y, float& _th);
	void setPose(float _x, float _y, float _th);
	float findDistance(Pose pos);
	float findAngleTo(Pose pos);
	Pose();
	~Pose();
};

