#pragma once
#include <iostream>
#include "SonarSensor.h"
#include "LaserSensor.h"
#include "RobotControl.h"
#include "Record.h"
#include "Pose.h"
#include "Path.h"
#include "Encryption.h"
#include "Node.h"
#include "RobotOperator.h"

using namespace std;

/**
* @File Menu.h
* @Date Aralik, 2018
* @Author Serhat Gun (serhatgunee@gmail.com)
*
* \brief Menu class
* \brief Bu class robotun hareketleri icin menu olusturur.
*/


class Menu
{
	Pose *pose;
	SonarSensor *sonar;
	LaserSensor* laser;
	Path *path;
	Record *record;
	RobotOperator *robotOp;
	RobotControl *robotCon;
	PioneerRobotAPI* pioneer;
	boolean connectionStatus;

public:
	/**
	* \brief Menu sinifinin constructor fonksiyonu
	*/
	Menu();
	/**
	* \brief Girdi t�rleri icin kontrol fonksiyonu
	* \param menuRange integer tipinde de�isken
	* \param input string tipinde de�isken
	*/
	int controlInput(int menuRange, string input);
	/**
	* \brief Menu sayfasini ekrana yazdirir ve kullanicinin secimine gore robotu kontrol eder
	*/
	void mainMenu();
	/**
	* \brief Bu fonksiyon baglanti menusunu ekrana yazdirir ve kullanicinin gerceklestirecegi islemi sorar.
	*/
	void connectionMenu();
	/**
	* \brief Bu fonksiyon hareket menusunu ekrana yazdirir ve kullanicinin gerceklestirecegi islemi sorar. 
	*/
	void motionMenu();
	/**
	* \brief Bu fonksiyon kullanicidan hiz degerini alir ve dondurur.
	* \return float hiz degiskeni dondurur
	*/
	float askSpeed();
	/**
	* \brief Bu fonksiyon string parametresini float parametresine donusturur.
	* \param input: string
	* \return data : float
	*/
	float controlFloat(string input);
	/**
	* \brief Bu fonksiyon string degerini integer degerine cevirir.
	* \param input : string
	* \return data : integer
	*/
	int controlInteger(string input);
	/**
	* \brief menu cllassinin yikici fonksiyonu
	*/
	~Menu();
};