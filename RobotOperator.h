#pragma once
#include <string>
#include <iostream>
#include "Encryption.h"
using namespace std;

class RobotOperator
{
public:
	RobotOperator();
	~RobotOperator();

	bool checkAccessCode(int);
	void print();

private:
	Encryption * enc;
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;

/**
	* \brief 4 rakamdan oluşan kodu, Encryption sınıfının fonksiyonu kullanılarak
	şifrelendirir.
	*/
	int encryptCode(int);
	/**
	* \brief 4 rakamdan oluşan kodu, Encryption sınıfının fonksiyonu kullanılarak şifresini
	çözdürür.
	*/
	int decryptCode(int);
};
