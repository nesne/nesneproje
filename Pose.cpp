#include "Pose.h"

Pose::~Pose()
{
}

float Pose::getX()
{
	return this->x;
}

void Pose::setX(float x)
{
	this->x = x;
}

float Pose::getY()
{
	return this->y;
}

void Pose::setY(float y)
{
	this->y = y;
}

float Pose::getTh()
{
	return this->z;
}

void Pose::setTh(float th)
{
	this->z = th;
}

bool Pose::operator==(const Pose& other)
{
	if (this->getX() == other.x && this->getY() == other.y && this->getTh() == other.z) {
		return true;
	}
	else
	{
		return false;
	}
}

Pose Pose::operator+(const Pose & other)
{
	Pose pose = Pose();
	pose.setX(this->getX() + other.x);
	pose.setY(this->getY() + other.y);
	pose.setTh(this->getTh() + other.z);

	return pose;
}

Pose Pose::operator-(const Pose & other)
{
	Pose pose = Pose();
	pose.setX(this->getX() - other.x);
	pose.setY(this->getY() - other.y);
	pose.setTh(this->getTh() - other.z);

	return pose;
}

Pose & Pose::operator+=(const Pose & other)
{
	this->setX(this->getX() + other.x);
	this->setY(this->getY() + other.y);
	this->setTh(this->getTh() + other.z);

	return *this;
}

Pose & Pose::operator-=(const Pose & other)
{
	this->setX(this->getX() - other.x);
	this->setY(this->getY() - other.y);
	this->setTh(this->getTh() - other.z);

	return *this;
}

bool Pose::operator<(const Pose & other)
{
	if (this->getX() < other.x && this->getY() < other.y && this->getTh() < other.z)  //debug yapilacak
	{
		return true;
	}
	else
		return false;
}

void Pose::getPose(float & _x, float & _y, float & _th)
{
	_x = this->getX();
	_y = this->getY();
	_th = this->getTh();
}

void Pose::setPose(float _x, float _y, float _th)
{
	this->setX(_x);
	this->setY(_y);
	this->setTh(_th);
}

float Pose::findDistance(Pose pos)
{
	return sqrt(pow((pos.x - this->getX()), 2) + pow((pos.y - this->getY()), 2));
}

float Pose::findAngleTo(Pose pos)
{
	return atan((pos.y - this->getY()) / (pos.x - this->getX()));
}

Pose::Pose()
{
	this->x = 0;
	this->y = 0;
	this->z = 0;
}
