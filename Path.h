#pragma once
#include "Node.h"
#include <iostream>
#include <string>

using namespace std;

/**
* @File Path.h
* @Date Aralik, 2018
* @Author Serhat Gun (serhatgunee@gmail.com)
*
* \brief Path class
*/

class Path
{
private:
	Node * tail;
	Node * head;
	int number;
public:
	/**
	*
	* @brief  Pose objesini pose listesinin sonuna ekler
	* @param  pose : Listeye eklenecek Pose objesi
	* @retval None
	*
	**/
	void addPos(Pose pose);
	/**
	*
	* @brief  Bulunan pozisyonlari uygun bir formatta ekrana yazdirir.
	* @retval None
	*
	**/
	void print();
	/**
	*
	* @brief  Verilen indeksteki konumu operator yukleme yontemiyle donduren fonksiyondur.
	* @param  i : konum indeksi
	* @retval *listofPoses_front -> pose objesi
	*
	**/
	Pose operator[](int i);
	/**
	*
	* @brief Verilen indeksteki konumu dondurur.
	* @param index : konum indeksi
	* @retval *listofPoses_front -> pose objesi
	*
	**/
	Pose getPos(int index);
	/**
	*
	* @brief  Verilen indeksteki konumu siler.
	* @param  index : konum indeksi
	* @retval *listofPoses_front -> pose objesi
	*
	**/
	bool removePos(int index);
	/**
	*
	* @brief  Verilen indekse verilen konum objesini ekler.
	* @param  index : konum indeksi
	* @param  pose  : eklenecek konum objesi
	* @retval
	*
	**/
	bool insertPos(int index, Pose pose);
	/**
	*
	* @brief Print fonksiyonun operator yukleme yontemiyle yazilmis fonksiyondur.
	* @param  i : konum indeksi
	* @retval *listofPoses_front : pose objesi
	*
	**/
	void operator << (Pose pose);
	/**
	*
	* @brief Verileri operator yukleme yontemiyle listenin sonuna ekleyen fonksiyondur.
	* @param  i : konum indeksi
	* @retval *listofPoses_front : pose objesi
	*
	**/
	void operator >> (Pose);
	/**
	* \brief Default constructor
	*/
	Path();
	/**
	* \brief Default destructor
	*/
	~Path();
};

