#include "Menu.h"
#include <string>

using namespace std;

Menu::Menu()
{
	this->robotOp = new RobotOperator();
	this->pioneer = new PioneerRobotAPI();
	this->robotCon = new RobotControl(pioneer);

	mainMenu();
}

int Menu::controlInput(int menuRange, string input)
{
	if (menuRange < 10)
	{
		if (input.length() > 1)
		{
			return 0;
		}
		else if (isdigit(input.at(0)))
		{
			int choice = atoi(input.c_str());
			if (choice > 0 && choice <= menuRange)
			{
				return choice;
			}
			else
			{
				return 0;
			}
		}
	}
	else
	{
		int choice;
		try
		{
			choice = atoi(input.c_str());
		}
		catch (const std::exception&)
		{
			return 0;
		}
		return choice;
	}
	return 0;
}


void Menu::mainMenu()
{
	while (true)
	{
		string input;
		cout << "Main Menu" << endl;
		cout << "1. Connection" << endl;
		cout << "2. Motion" << endl;
		cout << "3. Quit" << endl;
		cout << "Choose one : ";
		cin >> input;
		int choice = controlInput(4, input);
		switch (choice)
		{
		case 1:
			system("cls");
			cout << " Main Menu -> ";
			connectionMenu();
			break;
		case 2:
			system("cls");
			cout << "Main Menu -> ";
			motionMenu();
			break;
		case 3:
			cout << "Quitting the program " << endl;
			Sleep(200);
			return;
		default:
			cout << "Please enter input between 1 and 4" << endl;
			system("cls");
			break;
		}
	}
}
void Menu::connectionMenu()
{
	while (true)
	{
		string input;
		cout << "Connection Menu" << endl;
		cout << "1. Connect Robot" << endl;
		cout << "2. Disconnect Robot" << endl;
		cout << "3. Back" << endl;
		cout << "Choose one : ";
		cin >> input;
		int choice = controlInput(3, input);
		switch (choice)
		{
		case 1:

			if (!connectionStatus)
			{
				if (!pioneer->connect())
				{
					cout << "Could not connect..." << endl;
					cout << "Please open mobile sim if mobile sim opened that means unknown error" << endl;
					Sleep(200);
					system("cls");
					return;
				}
				else
				{
					connectionStatus = true;
					cout << "Robot Connection Established" << endl;
					Sleep(200);
					system("cls");
				}
			}
			else
			{
				cout << "Robot Connection Already Established" << endl;
				Sleep(200);
				system("cls");
			}
			break;
		case 2:
			if (connectionStatus)
			{
				if (!pioneer->disconnect())
				{
					cout << "Robot connection could not closed.Seems have Mobile Sim has a problem" << endl;
					Sleep(200);
					system("cls");
				}
				else
				{
					cout << "Robot connection closed" << endl;
					connectionStatus = false;
					Sleep(200);
					system("cls");
					exit(0);
				}
			}
			else
			{
				cout << "Robot connection already closed" << endl;
				Sleep(200);
				system("cls");
			}
			break;
		case 3:
			system("cls");
			return;
		default:
			cout << "Please enter input between 1 and 3" << endl;
			Sleep(200);
			system("cls");
			break;
		}
	}
}
void Menu::motionMenu()
{
	while (true)
	{
		if (connectionStatus)
		{
			string input;
			cout << "Motion Menu" << endl;
			cout << "1.  Move Robot" << endl;
			cout << "2.  Turn Left" << endl;
			cout << "3.  Turn Right" << endl;
			cout << "4.  Forward" << endl;
			cout << "5.  Backward" << endl;
			cout << "6.  Print Data" << endl;
			cout << "7.  Stop Robot" << endl;
			cout << "8.  Exit" << endl;
			cout << "Choose one : ";
			cin >> input;
			int choice = controlInput(10, input);
			float speedFloat = 0;
			int speedInteger = 0;
			float angle = 0;
			float distance = 0;
			switch (choice)
			{
			case 1:
				speedFloat = askSpeed();
				if ((int)speedFloat == 0)
				{
					cout << "You have entered wrong input don't enter 0 or char value" << endl;
					system("cls");
					Sleep(1000);
				}
				else
				{
					pioneer->moveRobot(speedFloat);
					system("cls");
				}
				break;
			case 2:
				robotCon->turnLeft();
				cout << "Turning Left" << endl;
				Sleep(1000);
				system("cls");
				break;
			case 3:
				robotCon->turnRight();
				cout << "Turning Right" << endl;
				Sleep(1000);
				system("cls");
				break;
			case 4:

				speedFloat = askSpeed();

				if ((int)speedFloat == 0)
				{
					cout << "You have entered wrong input don't enter 0 or char value" << endl;
					system("cls");
					Sleep(1000);
				}
				else
				{
					robotCon->stopTurn();
					robotCon->forward(speedFloat);
					cout << "Moving Forward" << endl;
					Sleep(1000);
					system("cls");
					break;
				}
				break;
			case 5:
				speedFloat = askSpeed();

				if ((int)speedFloat == 0)
				{
					cout << "You have entered wrong input don't enter 0 or char value" << endl;
					system("cls");
					Sleep(1000);
				}
				else
				{
					robotCon->backward(speedFloat);
					cout << "Moving Backward" << endl;
					Sleep(1000);
					system("cls");
					break;
				}
				break;
			case 6:
				robotCon->print();
				Sleep(2000);
				system("cls");
				break;
			case 7:
				robotCon->stopMove();
				cout << "Robot stopped" << endl;
				Sleep(1000);
				system("cls");
				break;
			case 8:
				system("cls");
				return;
			default:
				cout << "Please enter input between 1 and 10" << endl;
				Sleep(1000);
				system("cls");
				break;
			}
		}
		else
		{
			system("cls");
			cout << "Robot connection closed please open connection first" << endl;
			Sleep(1000);
			system("cls");
			return;
		}
	}
}

float Menu::askDistance()
{
	string input;
	float distance;
	cout << "Enter distance :" << endl;
	cin >> input;
	distance = controlFloat(input);
	return distance;
}
int Menu::askSafeMoveSpeed()
{
	string input;
	int speed;
	cout << "Enter speed :" << endl;
	cin >> input;
	speed = controlInteger(input);
	return speed;
}
float Menu::askSpeed()
{
	string input;
	float speed;
	cout << "Enter speed :" << endl;
	cin >> input;
	speed = controlFloat(input);
	return speed;
}
float Menu::controlFloat(string input)
{
	float data;
	try
	{
		data = stof(input);
	}
	catch (const std::exception&)
	{
		return 0;
	}
	return data;
}
int Menu::controlInteger(string input)
{
	int data;
	try
	{
		data = stoi(input);
	}
	catch (const std::exception&)
	{
		return 0;
	}
	return data;
}
Menu::~Menu()
{
}