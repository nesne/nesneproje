﻿#include "Path.h"

void Path::addPos(Pose pose)
{
	Node *temp = new Node();
	temp->pose = pose;
	temp->next = NULL;

	if (head == NULL)
	{
		head = temp;
		tail = temp;
		temp = NULL;
	}
	else
	{
		tail->next = temp;
		tail = temp;
	}
	this->number++;
}


void Path::print()
{

}


Pose Path::operator [](int index)
{
	Node *node = new Node();
	for (int i = 0; i <= index; i++)
	{
		if (i == index)
		{
			node->pose = this->head->pose;
			break;
		}
		else {
			node->pose = this->head->next->pose;
		}
	}
	return node->pose;
}


Pose Path::getPos(int index)
{
	return this->operator[](index);
}


bool Path::removePos(int index)
{
	if (index == 0)
	{
		Node *temp = new Node();
		temp = head;
		head = head->next;
		delete temp;
	}
	else if (index == this->number - 1)
	{
		Node *current = new Node();
		Node *previous = new Node();
		current = head;
		while (current->next != NULL)
		{
			previous = current;
			current = current->next;
		}
		tail = previous;
		previous->next = NULL;
		delete current;
	}
	else
	{
		Node *current = new Node();
		Node *previous = new Node();
		current = head;
		for (int i = 1; i < index; i++)
		{
			previous = current;
			current = current->next;
		}
		previous->next = current->next;
	}

	this->number--;

	return true;
}


bool Path::insertPos(int index, Pose pose)
{
	Node *pre = new Node();
	Node *cur = new Node();
	Node *temp = new Node();
	cur = head;
	for (int i = 1; i<index; i++)
	{
		pre = cur;
		cur = cur->next;
	}
	temp->pose = pose;
	pre->next = temp;
	temp->next = cur;

	this->number++;

	return true;
}

void Path::operator<<(Pose pose)
{
}

void Path::operator>>(Pose)
{
}

Path::Path()
{
	this->head = NULL;
	this->tail = NULL;
	this->number = 0;
}


Path::~Path()
{

}
