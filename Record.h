/**
* @File RobotControl.h
* @Date Aralik, 2018
* @Author Alaattin Dağlı (addidagli@gmail.com)
*
* \brief Record Sinifi
* \brief Bu sinif
*/
#pragma once
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
class Record
{
private:
	string fileName;
	fstream file;
public:
	/**
	* \brief Yazdırma ve okuma yapılacak dosya açar
	*/
	bool openFile(string fileName);
	/**
	* \brief Yazdırma ve okuma yapılacak dosya kapatır
	*/
	bool closeFile();
	/**
	* \brief Yazdırma ve okuma yapılacak dosya adını alır
	*/
	void setFileName(string name);
	/**
	* \brief Dosyadan bir satır veriyi okur.
	*/
	string readLine();
	/**
	* \brief Dosyaya bir satır veriyi yazar.
	*/
	bool writeLine(string str);
	/**
	* \brief Verileri dosyaya yazar
	*/
	Record& operator<<(string str);
	/**
	* \brief Verileri dosyadan alır.
	*/
	string& operator>>(string str);
	Record();
	~Record();
};
