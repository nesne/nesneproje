#pragma once
#include <iostream>
#include "PioneerRobotAPI.h"
#include "Pose.h"

/**
* @File RobotControl.h
* @Date Aralik, 2018
* @Author Emre Koc (emrekoc950@gmail.com)
*
* \brief RobotControl class
* \brief Bu class robotun hareketlerinin kontrolunu saglar.
*/
using namespace std;
class RobotControl
{
private:
	/**
	* \brief Pose* tipinde konumu tutan degisken
	*/
	Pose * position;
	/**
	* \brief PioneerRobotAPI classiyla aggregation iliskisi kurmak ve fonksiyonlarina erismek icin tanimlanan degisken
	*/
	PioneerRobotAPI* robotAPI = new PioneerRobotAPI();
	/**
	* \brief int tipinde state degiskeni
	*/
	int state;
	/**
	* \brief float tipinde sonarlari tutan dizi
	*/
	float sonars[16];
	/**
	* \brief float tipinde acilari tutan dizi
	*/
	float laserData[181];
public:
	/**
	* \brief Robotu sola donduren fonksiyon
	*/
	void turnLeft();
	/**
	* \brief Robotu saga donduren fonksiyon
	*/
	void turnRight();
	/**
	* \brief Robotun verilen hizda ilerlemesini saglayan fonksiyon
	* \param speed: hiz degiskeni
	*/
	void forward(float);
	/**
	* \brief Konumu, laser ve sonar sensorleri konsola yazdirir
	*/
	void print();
	/**
	* \brief Robotun verilen hizda gerilemesini saglayan fonksiyon
	* \param speed: hiz degiskeni
	*/
	void backward(float);
	/**
	* \brief Pozisyonu almamizi saglayan fonksiyon
	* \return Pose tipinde pozisyonu dondurur
	*/
	Pose getPose();
	/**
	* \brief Pozisyona deger atamamizi saglayan fonksiyon
	* \param Pozisyon tutan Pose
	*/
	void setPose(Pose);
	/**
	* \brief Robotun donusunu durduran fonksiyon
	*/
	void stopTurn();
	/**
	* \brief Robotu durduran fonksiyon
	*/
	void stopMove();
	/**
	* \brief Parametreli constructor fonksiyon
	*/
	RobotControl(PioneerRobotAPI *robotAPI);
	/**
	* \brief Default constructor
	*/
	RobotControl();
	/**
	* \brief Default destructor
	*/
	~RobotControl();
};

