#include "SonarSensor.h"
SonarSensor::SonarSensor()
{
	this->robotAPI->getSonarRange(this->ranges);
}

float SonarSensor::getRange(int index)
{
	return this->ranges[index];
}

float SonarSensor::getMax(int& index)
{
	float maximumValue = this->ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (maximumValue < this->ranges[i])
		{
			maximumValue = this->ranges[i];
		}
	}
	return maximumValue;
}

float SonarSensor::getMin(int& index)
{
	float minimumValue = this->ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (minimumValue > this->ranges[i])
		{
			minimumValue = this->ranges[i];
		}
	}
	return minimumValue;
}
void SonarSensor::updateSensor(float *ranges)
{
	for (int i = 0; i < 16; i++)
	{
		this->ranges[i] = ranges[i];
	}
}
float SonarSensor::operator[](int i)
{
	return this->ranges[i];
}
float SonarSensor::getAngle(int index)
{
	switch (index)
	{
	case 0:
	case 15:
		return -90;
	case 1:
	case 14:
		return -50;
	case 2:
	case 13:
		return -30;
	case 3:
	case 12:
		return -10;
	case 4:
	case 11:
		return 10;
	case 5:
	case 10:
		return 30;
	case 6:
	case 9:
		return 50;
	case 7:
	case 8:
		return 90;
	default:
		return -1;
		break;
	}
}

SonarSensor::~SonarSensor()
{
}
