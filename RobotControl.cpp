#include "RobotControl.h"

void RobotControl::turnLeft() {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}

void RobotControl::turnRight() {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}

void RobotControl::forward(float speed) {
	robotAPI->moveRobot(speed);
}

void RobotControl::print() {
	cout << "MyPose is (" << robotAPI->getX() << "," << robotAPI->getY() << "," << robotAPI->getTh() << ")" << endl;
	cout << "Sonar ranges are [ ";
	robotAPI->getSonarRange(sonars);
	for (int i = 0; i < 16; i++) {
		cout << sonars[i] << " ";
	}
	cout << "]" << endl;
	cout << "Laser ranges are [ ";
	robotAPI->getLaserRange(laserData);
	for (int i = 0; i < 181; i++) {
		cout << laserData[i] << " ";
	}
	cout << "]" << endl;
}

void RobotControl::backward(float speed) {
	speed *= -1;
	robotAPI->moveRobot(speed);
}

Pose RobotControl::getPose() {
	return *position;
}


void RobotControl::setPose(Pose pose) {
	this->position = &pose;
	robotAPI->setPose(pose.getX(), pose.getY(), pose.getTh());
}

void RobotControl::stopTurn() {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
}

void RobotControl::stopMove() {
	robotAPI->stopRobot();
}

RobotControl::RobotControl()
{
}

RobotControl::RobotControl(PioneerRobotAPI *robot)
{
	this->robotAPI = robot;
	this->position = new Pose();
}

RobotControl::~RobotControl()
{
}
