#pragma once
#include "PioneerRobotAPI.h"
/**
* @File SonarSensor.h
* @Date Aralik, 2018
* @Author Seda Kaynar (sedakynr96@gmail.com)
* \brief SonarSensor Sinifi
*/

class SonarSensor
{
private:
	float ranges[16];
	PioneerRobotAPI *robotAPI;
public:
	/**
	* \brief  i. �ndeksine sahip sensorun mesafe bilgisini dondurur.
	* \param int index : konum
	* \return menzil bilgisini dondurur.
	*/
	float getRange(int index);
	/**
	* \brief  �ndeksin maximum bilgisini dondurur.
	* \param int index : konum
	* \return maximum degeri verir.
	*/
	float getMax(int& index);
	/**
	* \brief  �ndeksin minimum bilgisini dondurur.
	* \param int index : konum
	* \return minimum degeri verir.
	*/
	float getMin(int& index);
	/**
	* \brief   Robota ait guncel sensor mesafe degerlerini, ranges dizisine yukler.
	* \param float ranges[] : menzil degerlerini dizide tutar.
	*/
	void updateSensor(float *ranges);
	/**
	* \brief   indeksi verilen sensor degerini dondurur. getRange(i) ile benzer fonksiyonu gercekler.
	* \param int i : ranges sayi degeri
	* \return ranges[i] degerini dondurur.
	*/
	float operator[](int i);
	/**
	* \brief �ndeksi verilen sensorun ac� degerini dondurur.
	* \param int index : konum
	*/
	float getAngle(int index);
	/**
	* \brief sonarSensor classinin yapici fonksiyondur.
	*/
	SonarSensor();
	/**
	* \brief sonarSensor classinin yikici fonksiyondur.
	*/
	~SonarSensor();
};

